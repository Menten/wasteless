# Wasteless CSS

No Class, No Style, No Charm

## Key features

- A basic grid
- No theme specific styles
- Completely classless
- With accessibility (a11y) in mind
- Responsive, widescreen- and mobile-friendly
- Standards compliant

## Browser support

- Latest Firefox and other browsers

## Usage

see [INSTALL](INSTALL.md)

## Frequently-Asked-Questions

see [FAQ](FAQ.md)

## License

[EUPL](LICENSE)
