# How to contribute

## **Bugs, patches and features**

### **Did you find a bug?**

- **Open up an issue if the bug affects you**
- **DO NOT open up an issue if the bug is a security vulnerability, and instead contact author directly.**

- **Ensure the bug was not already reported** by searching under [Issues](https://gitlab.com/Menten/wasteless/issues).

- If you're unable to find an open issue addressing the problem, [open a new one](https://gitlab.com/Menten/wasteless/issues/new). Be sure to include a **title and clear description**, as much relevant information as possible, and an **procedure steps** demonstrating the expected behavior that is not occurring.

### **Did you write a patch that fixes a bug?**

- Open a new pull request with the patch.

- Ensure the PR description clearly describes the problem and solution. Include the relevant issue number if applicable.

- Try to have clear and consistent code. See our Styleguide code conventions below.

### **Did you fix whitespace, format code, or make a purely cosmetic patch?**

Changes that are cosmetic in nature such as grammar or whitespace are also appreciated, please send pull request.

### **Do you intend to add a new feature or change an existing one?**

- Suggest your change with a [new issue](https://gitlab.com/Menten/wasteless/issues/new) and start writing code.

## **Issue Triage Policies**

### **Outdated issues**

For issues that haven't been updated in the last 3 months the "Awaiting Feedback" label should be added to the issue. After 14 days, if no response has been made by anyone on the issue, the issue should be closed.

If they respond at any point in the future, the issue can be considered for reopening. If we can't confirm an issue still exists in recent versions, we're just adding noise to the issue tracker.

### **Duplicates**

Before opening a new issue, make sure to search for keywords and verify your issue isn't a duplicate.

Checking for and/or reporting duplicates when you notice them.

All things held equal, the earliest issue should be considered the canonical version. If one issue has a better title, description, and/or more comments and positive reactions, it should be prioritized over earlier issues even if it's a duplicate.

### **Lean toward closing**

We simply can't satisfy everyone. We need to balance pleasing users as much as possible with keeping the project maintainable.

    If the issue is a bug report without reproduction steps or version information, close the issue and ask the reporter to provide more information.
    If we're definitely not going to add a feature/change, say so and close the issue.

### **Label issues as they come in**

When an issue comes in, it should be triaged and labeled. Issues without labels are harder to find and often get lost.

### **Take ownership of issues you've opened**

Sort by "Author: your username" and close any issues which you know have been fixed or have become irrelevant for other reasons. Label them if they're not labeled already.

### **Questions/support issues**

If it's a question, or something vague that can't be addressed by the development team for whatever reason, close it and direct them to the relevant support resources we have (e.g. our Matrix channel or emailing Support).

### **New labels**

If you notice a common pattern amongst various issues (e.g. a new feature that doesn't have a dedicated label yet), suggest adding a new label in chat.

@Menten is the "Label King", make sure he approves of a label before adding it. This way we don't have a bunch of repetitive/unused/inconsistent labels.

### **Automated triaging of stale issues and merge requests**

We'd like to ask you to help us out and determine whether these issues should be reopened.

## **Workflow labels**

|          Label           |                                            Explanation                                             |
| ------------------------ | -------------------------------------------------------------------------------------------------- |
| accepting merge requests | Issues opened for contribution from the Community.                                                 |
| documentation            | Covers our efforts to continuously improve and maintain the quality of documentation.              |
| in dev                   | Issues that are actively being worked on by a developer.                                           |
| IE                       | Issues affecting Microsoft Internet Explorer 11 running on Windows.                                |
| Microsoft Edge           | Issues affecting Microsoft Edge web browser running on Windows 10 or higher.                       |
| P1                       | Priority 1 - Urgent: The current release.                                                          |
| P2                       | Priority 2 - High: The next release.                                                               |
| P3                       | Priority 3 - Medium: Within the next 3 releases (approx one quarter).                              |
| P4                       | Priority 4 - Low: Anything outside the next 3 releases (approx beyond one quarter).                |
| QA                       | QA - related changes.                                                                              |
| S1                       | Severity 1 - Blocker.                                                                              |
| S2                       | Severity 2 - Critical.                                                                             |
| S3                       | Severity 3 - Major.                                                                                |
| S4                       | Severity 4 - Low.                                                                                  |
| UX                       | Covers new or existing functionality that needs a design proposal.                                 |
| accessibility            | Issues with preventing screen readers and other accessibility tools from working correctly.        |
| auto closed              | Relates to issues that have been closed in accordance with our Issue Triage Policies.              |
| auto updated             | Issues that have been updated in accordance with our Issue Triage Policies and marked for closure. |
| awaiting feedback        | Issues that are blocked until more feedback is received from the reporter.                         |
| blocked                  | Issues that are blocked until another issue has been completed.                                    |
| breaking change          | A backwards-incompatible change that will need to be made with the next major release.             |
| demo                     | Issues encountered while setting up demos.                                                         |
| deprecation              | Issues about deprecation of old features.                                                          |
| development guidelines   | Apply this label for new development guidelines or related changes.                                |
| docs                     | An idea or proposal for missing documentation content.                                             |
| encoding                 | Issues related to encoding handling.                                                               |
| enhancement              | Issues that propose improvements to existing features.                                             |
| feature proposal         | Issues that propose new features.                                                                  |
| forking                  | Issues related to creating, maintaining and deleting forks.                                        |
| frequently duplicated    | Issues which are frequently duplicated, tracked for ease of finding duplicates.                    |
| installation             | Issues related to installing.                                                                      |
| merge requests           | Issues related to merge requests and code review.                                                  |
| needs investigation      | Relates to issues raised by the community that need further investigation to triage. Please feel free to help investigate! |
| no priority              | Added to issues with no priority label. Please remove once a priority label has been added.        |
| no severity              | Added to issues with no severity label. Please remove once a severity label has been added.        |
| popular proposal         | Feature proposals deemed popular by our Issue Triage Policies will have this label applied.        |
| potential proposal       | Label added by triage tools to mark interesting feature proposals for consideration.               |
| regression               | Issues with this label are regressions from the previous non-patch release.                        |
| responsive               | Pertaining to responsive web design which is aimed at crafting sites to provide an optimal viewing and interaction experience—easy reading and navigation with a minimum of resizing, panning, and scrolling—across a wide range of devices. |
| security                 | Issues related to security. Please report vulnerabilities responsibly and contact author directly. |
| technical debt           | Issues related to things that need improvement and that have been left behind due to high velocity of development. |
| to schedule              | Issues that need to be scheduled for a future release.                                             |
| wontfix                  | Issues that will be not handled.                                                                   |

## **Styleguide**

### **Source Code Layout**

- Use UTF-8 as the source file encoding.

- Use four spaces per indentation level (aka soft tabs). No hard tabs.

- Use Unix-style line endings. (\*BSD/Solaris/Linux/macOS users are covered by default, Windows users have to be extra careful.)

  - If you're using Git you might want to add the following configuration setting to protect your project from Windows line endings creeping in:

    ```bash
    git config --global core.autocrlf true
    ```

- Limit lines to 80 characters.

- Avoid trailing whitespace.

- End each file with a newline.

- Don't use block comments. They are not as easy to spot as regular comments.

- Code should be written in US English.

### **Naming**

- Name identifiers in English.

- Filenames should use the `lowercase-hyphenated` format rather than `snake_case` or `camelCase`.

### **Formatting**

- Each property should each get its own line, and there should be a space between the property and its value.

- Single-line rulesets are not typically recommended.

- Colors should use `hsla()` functional notations where possible.

- Always include semicolons after every property.

- The shorthand form should be used, and should use lower case letters to differentiate between letters and numbers, for properties that support it.

- Omit length units on zero values, they're unnecessary and not including them is slightly more performant.

- Do not use any selector prefixed with `js-` for styling purposes. These selectors are intended for use only with JavaScript to allow for removal or renaming without breaking styling.

- Before adding a new variable, guarantee:

1. There isn't already one
2. There isn't a similar one we can use instead.

- Don't use ID or class selectors in CSS.

### **Comments**

- Write self-documenting code and ignore the rest of this section. Seriously!

- Write comments in English.

- Use one space between the leading character of the comment and the text of the comment.

- Comments longer than a word are capitalized and use punctuation. Use one space after periods.

- Avoid superfluous comments.

- Keep existing comments up-to-date. An outdated comment is worse than no comment at all.

- Avoid writing comments to explain bad code. Refactor the code to make it self-explanatory.

### **Annotations**

- Annotations should usually be written on the line immediately above the relevant code.

- The annotation keyword is followed by a colon and a space, then a note describing the problem.

- Use `TODO` to note missing features or functionality that should be added at a later date.

- Use `FIXME` to note broken code that needs to be fixed.

- Use `OPTIMIZE` to note slow or inefficient code that may cause performance problems.

- Use `HACK` to note code smells where questionable coding practices were used and should be refactored away.

- Use `REVIEW` to note anything that should be looked at to confirm it is working as intended.

### Fixing issues

If you want to automate changing a large portion of the codebase to conform to the CSS style guide, you can use CSSComb. First install Node and NPM, then run npm install csscomb -g to install CSSComb globally (system-wide). Run it in the project directory with csscomb app/assets/stylesheets to automatically fix issues with CSS.

Note that this won't fix every problem, but it should fix a majority.
