# Usage

* Get the latest version
  * <https://gitlab.com/Menten/wasteless/raw/master/wastele.css>

* Place in the same location as your website's stylesheet

* Add this to the top of your stylesheet

  ```css
  @import 'wastele.css';
  ```

## Basic HTML template

  Check [MANIFEST](MANIFEST) for included files to use
