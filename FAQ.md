# FAQ

## Why the name

You can interpret the project name as follows:

- wasteless (incapable of being used up)
- wasteless (without waste)
- waste less (code, time...)
- waste (destroy) less (pre-processor)

## Do you have a demo

The grid works like this:

```text
width < 768px    width > 768px                           width > 992px
+-----------+    +----------------------------------+    +------------------------------------------------+
|           |    |         header                   |    |              header                            |
|           |    +------------+---------------------+    +-----------------+------------------+-----------+
|  article  |    |            |                     |    |                 |                  |           |
|           |    |            |                     |    |                 |                  |           |
+-----------+    |  nav       |  article            |    |                 |                  |           |
|           |    |            |                     |    |  nav            |  article         |  aside    |
|           |    |            |                     |    |                 |                  |           |
|  nav      |    +------------+---------------------+    |                 |                  +-----------+
|           |    |            |                     |    |                 |                  |           |
|           |    |  section   |  aside              |    |                 |                  |  section  |
|           |    |            |                     |    |                 |                  |           |
|           |    +------------+---------------------+    +-----------------+------------------+-----------+
|           |    |         footer                   |    |              footer                            |
+-----------+    +----------------------------------+    +------------------------------------------------+
|           |
|           |
|  aside    |
|           |
|           |
|           |
+-----------+
|  section  |
+-----------+
```

## Can you provide ports to Sass, Less, etc

There's absolutely no reason to have separate ports for these. They are just CSS supersets and can import CSS directly.

## Can you add a CDN link

No, for more information visit: <https://www.peppercarrot.com/article390/my-fight-against-cdn-libraries>

## Do you have a Code of Conduct

No, for more information visit: <https://shiromarieke.github.io/coc.html>
